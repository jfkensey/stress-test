#!/usr/bin/env python
# -*- coding: utf-8 -*-
# vim: set fileencoding=utf-8 :

from __future__ import print_function
from __future__ import unicode_literals
import threading
import time
import Queue
import urllib2
import os.path
import os
import random
import argparse
import re
import sys

from datetime import datetime
import signal

import requests
import requests.exceptions

from colorama import Fore, Style
import colorama
import anydbm
import tempfile 

# color handler
colorama.init(autoreset=True)

# Queue control 
q = Queue.Queue(30)
qLock = threading.Lock()

dccnt = 0
act_proc_cnt = 0

total_cnt = 0
total_requests = 0
total_success = 0
total_response_over_sec = 0
optimal_max_time_response = 1 # in sec
max_response_time = 0
total_response_time = 0
delta_time = 0
http_error_code_counter = {}


exit = False




def _signal_handler(signum, frame):
    global exit
    exit = True
    print(Fore.YELLOW + '>>>>>>>>>  Exiting...')


class ReadURL(threading.Thread):
    _db = None
    _tmp_file_name = None
    
    def __init__(self, spath):
        """
        spath: path to file with the URLs lists
        """
        threading.Thread.__init__(self)
        self.spath = spath
        print( "Entry in ReadURL" )
        
        
    def _open_temp(self):
        temp_name = "%s_%s_%s" % (
            str(random.triangular(0, 25000000)),
            str(time.time()),
            str(random.SystemRandom(time.time()).random()) 
        )

        self._tmp_file_name = os.path.join("/tmp/", temp_name)

        self._db = anydbm.open(
            self._tmp_file_name, 
            "c" 
        )

    def run(self):
        global exit # global exit flag
        try:
            # open temp file
            self._open_temp()
            
            print( self._tmp_file_name )
            
            ndx = 0
            # loading urls from file
            print( Fore.YELLOW + "loading file %s" % self.spath)
            with open(self.spath) as f:
                for l in f:
                    self._db[str(ndx)] = l.strip().split("\n")[0]
                    ndx += 1
            # ----------------------
            
            # putting values into the queue until the exit will be required
            # using  Ctrl + C or TERM Signal
            if len(self._db) > 0:
                
                # adding random values from url list
                while True:
                    if exit:
                        break
                    # if the queue is full wait for space
                    while q.full():
                        print(Fore.BLUE + "ReadURL: max queue...")
                        time.sleep(random.triangular(0.3, 1))
                        # TODO: add a related process checking to finish if it
                        #       is not alive

                    # lock
                    qLock.acquire()
                    
                    # getting the random URL value
                    cadUrl = self._getRandomURL()
                    # putting value to the queue
                    q.put(cadUrl)
                    
                    # unlock
                    qLock.release()
            else:
                print(Fore.YELLOW + ">>> WARN : there is not data into the file")
            print(Fore.YELLOW + "finishing ReadURL...")
        except Exception, ex:
            print("run exception:", ex)
            
        # closing/ removing temp file
        try:
            self._db.close()
        except:
            pass
        finally:
            try:
                if os.path.isfile(self._tmp_file_name):
                    os.unlink(self._tmp_file_name)
            except Exception, ex:
                #TODO: log any event
                print(ex)
            
        # forcing exit to all processes related: there not will be any 
        # value into the queue
        self.exit()

    def exit(self):
        """
        public interface to finish the process
        """
        global exit
        exit = True

    def _getRandomURL(self):
        """
        get a random value form de url list
        """
        _rnd = int(random.SystemRandom().triangular(0, len(self._db)))
        return self._db[str(_rnd)]


class Stresstest(threading.Thread):

    class CallURL(threading.Thread):
        _url = None
        ntotal = 0

        def __init__(self, url, new_cnx_level):
            global act_proc_cnt
            threading.Thread.__init__(self)
            self._url = url
            self.new_cnx_level = new_cnx_level

            # count a total of processes
            qLock.acquire()
            act_proc_cnt = act_proc_cnt + 1
            qLock.release()

        def _getUrl(self, url):
            # random value to know if the call use sticky-var
            _ndx_new_cnx = random.SystemRandom().triangular(0, 100)
            if _ndx_new_cnx < self.new_cnx_level:
                # random value for the sticky-var
                _stick = int(random.SystemRandom().triangular(0, 99565535))
                
                # check if the url has query string
                _retmp = re.match(r'^(.*)\?(.+)?$', url.strip())
                
                _tmpl = "%s?_vsq1=%d"
                if _retmp is not None:
                    # the string has character "?"
                    if _retmp.groups(2) is not None:
                        # example: http:domain?q=1&q2
                        _tmpl = "%s&_vsq1=%d"
                    else:
                        # example: http:domain?
                        _tmpl = "%s_vsq1=%d"
                _cadtmp = _tmpl % (url,_stick)
            else:
                _cadtmp = self._url
            return _cadtmp

        def run(self):
            global act_proc_cnt # processes stack counter
            global total_cnt # a total interaction counter
            global exit
            global total_requests
            global total_success
            global max_response_time
            global total_response_time
            global delta_time
            global total_response_over_sec
            global optimal_max_time_response
            global http_error_code_counter
            
            try:
                # --------------------
                # lock
                qLock.acquire()
                
                # getting random url string
                _cadtmp = self._getUrl(self._url)

                print("request to ", \
                    Fore.GREEN + _cadtmp)
                
                timeA = time.time()
                # unlock
                qLock.release()
                # --------------------

                # calling the url
                furl = requests.get(_cadtmp)
                
                # --------------------
                qLock.acquire()
                
                timeD = time.time() - timeA
                
                total_requests = total_requests + 1
                
                if timeD > max_response_time:
                    max_response_time = timeD 
                
                # TODO: the value of optimal_max_time_response should be given
                #       by the user in command line
                if timeD > optimal_max_time_response:
                    total_response_over_sec = total_response_over_sec +1
                    
                    
                total_response_time = total_response_time + timeD
                delta_time = total_response_time / total_requests
                
                qLock.release()
                # --------------------
                
                # if the response is not a 200 code raise and error
                if not furl.status_code == requests.codes.ok:
                    furl.raise_for_status()

                qLock.acquire()
                total_success= total_success + 1
                qLock.release()


            except requests.exceptions.ConnectionError, ex:
                print(Fore.RED + "ConnectionError: %s" % (ex.message))
                
                status_code = "ConnectionError"
                if http_error_code_counter.has_key(status_code):
                    http_error_code_counter[status_code] = http_error_code_counter[status_code] + 1
                else:
                    http_error_code_counter.setdefault(status_code, 1)

            except requests.exceptions.Timeout, ex:
                print(Fore.RED + "Timeout: %s" % (ex.message))

                status_code = "Timeout"
                if http_error_code_counter.has_key(status_code):
                    http_error_code_counter[status_code] = http_error_code_counter[status_code] + 1
                else:
                    http_error_code_counter.setdefault(status_code, 1)

            except requests.exceptions.TooManyRedirects, ex:
                print(Fore.RED + "TooManyRedirects: %s" % (ex.message))

                status_code = "TooManyRedirects"
                if http_error_code_counter.has_key(status_code):
                    http_error_code_counter[status_code] = http_error_code_counter[status_code] + 1
                else:
                    http_error_code_counter.setdefault(status_code, 1)

            except requests.exceptions.HTTPError, ex:
                print(Fore.RED + "HTTPError: %s" % (ex.message))
                status_code = ex.response.status_code
                
                if http_error_code_counter.has_key(status_code):
                    http_error_code_counter[status_code] = http_error_code_counter[status_code] + 1
                else:
                    http_error_code_counter.setdefault(status_code, 1)

                time.sleep(0.5)
                #             except urllib2.URLError, ex:
                #                 print(Fore.RED + "URLError: [%s] %s" % (ex.code, ex.reason))
                # 
                #                 status_code = "ConnectionError"
                #                 if http_error_code_counter.has_key(status_code):
                #                     http_error_code_counter[status_code] = http_error_code_counter[status_code] + 1
                #                 else:
                #                     http_error_code_counter.setdefault(status_code, 1)
                
            except Exception, ex:
                print(Fore.RED + "Error: %s" % (ex.message))

                status_code = "Other"
                if http_error_code_counter.has_key(status_code):
                    http_error_code_counter[status_code] = http_error_code_counter[status_code] + 1
                else:
                    http_error_code_counter.setdefault(status_code, 1)

            qLock.acquire()
            act_proc_cnt = act_proc_cnt - 1
            
            #if exit:
            #    print("leaving %s process(es) " % act_proc_cnt)
            qLock.release()

    def __init__(self, max_process=10, new_cnx_level=30):
        threading.Thread.__init__(self)
        global act_proc_cnt
        self.maxproc = max_process
        self.new_cnx_level = new_cnx_level

    def run(self):
        global exit # exit flag
        global act_proc_cnt # processes counter
        global total_requests
        global total_success
        global max_response_time
        global delta_time
        global total_response_over_sec
        global optimal_max_time_response
        global http_error_code_counter

        time_start_test = time.time()
        _now = datetime.now()
        
        print(Fore.YELLOW + "max process -> %d" % act_proc_cnt)

        try:
            act_proc_cnt = 0
            _fnd = False # local exit flag
            while not _fnd:
                if q.empty() and exit:
                    # wait until the queue is empty to exit
                    _fnd = True
                elif q.empty() or (act_proc_cnt > self.maxproc-1):
                    # wait for values in the queue or
                    # wait the actual bunch of processes alive (under the max-proc-limit)
                    # finish to add more if there are
                    time.sleep(random.SystemRandom().triangular(0.5, 2))
                else:
                    # block 
                    qLock.acquire()
                    
                    # get from queue
                    cadUrl = q.get()
                    q.task_done()
    
                    # unlock
                    qLock.release()
    
                    if not exit:
                        # run a subprocess call the URL
                        prc = self.CallURL(cadUrl, self.new_cnx_level)
                        prc.start()
                    
            qLock.acquire()
            print(Fore.YELLOW + "StressTest --> Exit")
            qLock.release()
        except Exception, ex:
            print("main run exception:", ex)
        
        time.sleep(random.SystemRandom().triangular(0.5, 2))
        
        print("run time: %s" % (datetime.now() - _now))
        print("Total requests: %s" % total_requests)
        print("Total success: %s" % total_success)
        print("Max response time in sec: %s" % max_response_time)
        print("Avg response time in sec: %s" % delta_time)
        print("requests per sec: %s" % (
            total_requests / (time.time() - time_start_test)
        ))
        print(
            "Total requests with time response > %s sec: %s" % (
                optimal_max_time_response,
                total_response_over_sec
                )
            )
        print(
            "Total requests with time response <= %s sec: %s" % (
                optimal_max_time_response,
                total_requests - total_response_over_sec
                )
            )
        
        print("Summary Errors:")
        for k in  http_error_code_counter.keys():
            print(
                "%s : %s" % (
                    k,
                    http_error_code_counter[k]
                )
            )

def main():
    parse = argparse.ArgumentParser()
    
    parse.add_argument(
        "--url-list",
        help="File with the urls to be requests",
        required=True)
    
    parse.add_argument(
        "--max-process",
        help="Maximun number of process to be created",
        required=False,
        default="10")
    
    parse.add_argument(
        "--level-new-cnx",
        help="level from 0 to 100, it is the probability of new connection adding"+
            "sticky vars to the urls",
        required=False,
        default="30")
    
    args = parse.parse_args()
    if not os.path.isfile(args.url_list):
        print("file %s not found" % args.url_list)
        sys.exit(1)
    
    max_process = int(args.max_process)
    new_cnx_level = int(args.level_new_cnx)
    
    # catch signal to terminate the process
    signal.signal(signal.SIGTERM, _signal_handler)
    signal.signal(signal.SIGUSR1, _signal_handler)
    signal.signal(signal.SIGUSR2, _signal_handler)
    signal.signal(signal.SIGALRM, _signal_handler)
    signal.signal(signal.SIGINT, _signal_handler)
    signal.signal(signal.SIGQUIT, _signal_handler)
    
    # initialize URLs queue 
    rU = ReadURL(args.url_list)
    
    # initialize Requests process
    sT = Stresstest(max_process, new_cnx_level)
    
    # start processes
    rU.start()
    sT.start()
    
    # check if the processes are alive to
    # and wait for their finish
    while rU.is_alive() or sT.is_alive():
        if rU.is_alive():
            rU.join(1)
    
        if sT.is_alive():
            sT.join(1)
            
        # time.sleep(0.5)


if __name__ == "__main__":
    main()